#![no_std]
#![no_main]

use defmt_rtt as _;
use panic_probe as _;

use bsp::entry;
use bsp::hal::{
    clocks::{init_clocks_and_plls, Clock},
    gpio, pac,
    sio::Sio,
    spi::Spi,
    watchdog::Watchdog,
};
use embedded_hal::digital::v2::InputPin;
use rp_pico as bsp;

use fugit::RateExtU32;
use smart_leds::{brightness, hsv::RGB};
use smart_leds_trait::SmartLedsWrite;
use ws2812_blocking_spi::*;

use defmt::info;

mod faces;
use faces::{Creature, Face, X_PIXELS, Y_PIXELS};

#[entry]
fn main() -> ! {
    info!("Program start");
    let mut pac = pac::Peripherals::take().unwrap();
    let core = pac::CorePeripherals::take().unwrap();
    let mut watchdog = Watchdog::new(pac.WATCHDOG);
    let sio = Sio::new(pac.SIO);

    // External high-speed crystal on the pico board is 12Mhz
    let external_xtal_freq_hz = 12_000_000u32;
    let clocks = init_clocks_and_plls(
        external_xtal_freq_hz,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    let mut delay = cortex_m::delay::Delay::new(core.SYST, clocks.system_clock.freq().to_Hz());

    let pins = bsp::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    // Init SPI bus. Connect LEDs to pin 7 (MOSI)
    let _spi_sclk = pins.gpio6.into_mode::<gpio::FunctionSpi>();
    let _spi_mosi = pins.gpio7.into_mode::<gpio::FunctionSpi>();
    let _spi_miso = pins.gpio8.into_mode::<gpio::FunctionSpi>();
    let spi = Spi::<_, _, 8>::new(pac.SPI0);
    let spi = spi.init(
        &mut pac.RESETS,
        clocks.peripheral_clock.freq(),
        2400.kHz(),
        &embedded_hal::spi::Mode {
            polarity: embedded_hal::spi::Polarity::IdleLow,
            phase: embedded_hal::spi::Phase::CaptureOnFirstTransition,
        },
    );
    let mut spi = Ws2812BlockingWriter::new(spi);

    // Tiltswitch to detect shaking
    let tiltswitch = pins.gpio16.into_pull_down_input();

    let mut creature = Creature::new(Face::Sleepy);

    loop {
        // Read the tiltswitch and update the shake history
        let shake_state = tiltswitch.is_high().unwrap();
        creature.next(shake_state);

        if creature.is_shaking() {
            creature.face = match creature.face {
                Face::Sleepy => Face::Surprise,
                Face::Happy | Face::HappyBlink | Face::Neutral => Face::Fun,
                other => other,
            }
        } else {
            creature.face = match creature.face {
                Face::Fun | Face::Happy => Face::Happy,
                Face::Sleepy => Face::Sleepy,
                _ => Face::Neutral,
            };
        }

        let led_strip_data = serialize(creature.face());

        spi.write(brightness(led_strip_data.iter().cloned(), 32))
            .unwrap();
        delay.delay_ms(10);
    }
}

fn serialize(
    pixels: [(usize, usize, u8, u8, u8); X_PIXELS * Y_PIXELS],
) -> [RGB<u8>; X_PIXELS * Y_PIXELS] {
    let mut matrix: [[(u8, u8, u8); X_PIXELS]; Y_PIXELS] = [[(0, 0, 0); X_PIXELS]; Y_PIXELS];
    let mut strip: [RGB<u8>; X_PIXELS * Y_PIXELS] = [RGB::new(0, 0, 0); X_PIXELS * Y_PIXELS];

    for pixel in pixels {
        let (x, y, r, g, b) = pixel;
        matrix[y][x] = (r, g, b);
    }

    for (rownum, row) in matrix.iter().enumerate() {
        for (pixelnum, pixel) in row.iter().enumerate() {
            let (r, g, b) = pixel;

            // This is done because the strip is wired on a zigzag.
            if rownum % 2 == 0 {
                // If the row is even, calculate the position on the strip
                strip[X_PIXELS * rownum + pixelnum] = RGB::new(*r, *g, *b);
            } else {
                // If the row is odd, calculate the mirrored version of it on the strip
                strip[X_PIXELS * (rownum + 1) - (pixelnum + 1)] = RGB::new(*r, *g, *b);
            }
        }
    }

    return strip;
}
