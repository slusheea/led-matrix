use crossterm::{
    event::{self, DisableMouseCapture, EnableMouseCapture, Event, KeyCode},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use hex::FromHex;
use std::{
    collections::HashMap,
    error::Error,
    fs::write,
    io,
    time::{Duration, Instant},
};
use tui::{
    backend::{Backend, CrosstermBackend},
    layout::{Constraint, Direction, Layout, Rect},
    style::{Color, Modifier, Style},
    text::{Span, Spans, Text},
    widgets::{
        canvas::{Canvas, Rectangle},
        Block, Borders, Clear, Paragraph,
    },
    Frame, Terminal,
};
use unicode_width::UnicodeWidthStr;

enum Mode {
    Drawing,
    ColorChange,
    Save,
    Quit,
}

struct App {
    // Cursor position
    x: f64,
    y: f64,

    // Editing mode
    mode: Mode,

    // User hex color input string, rbg parsed value and save file name.
    color_input: String,
    color: (u8, u8, u8),
    filename: String,

    // Holds the drawing itself
    pixels: HashMap<(i64, i64), (u8, u8, u8)>,
    // <(pixel coordinates),  (pixel rbg color value)> // <(x, y), (r, g, b)>
}

impl App {
    fn new() -> Self {
        Self {
            x: 1.0,
            y: 1.0,

            mode: Mode::Drawing,

            color_input: String::new(),
            color: (0xFF, 0xFF, 0xFF),
            filename: String::new(),

            pixels: HashMap::new(),
        }
    }
}

const X_SIZE: f64 = 15.0;
const Y_SIZE: f64 = 6.0;

fn main() -> Result<(), Box<dyn Error>> {
    // setup terminal
    enable_raw_mode()?;
    let mut stdout = io::stdout();
    execute!(stdout, EnterAlternateScreen, EnableMouseCapture)?;
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    // create app and run it
    let tick_rate = Duration::from_millis(250);
    let mut app = App::new();

    // Create black pixel grid
    for i in 0..X_SIZE as i64 {
        for j in 0..Y_SIZE as i64 {
            app.pixels.insert((i + 1, j + 1), (0, 0, 0));
        }
    }

    let res = run_app(&mut terminal, app, tick_rate);

    // restore terminal
    disable_raw_mode()?;
    execute!(
        terminal.backend_mut(),
        LeaveAlternateScreen,
        DisableMouseCapture
    )?;
    terminal.show_cursor()?;

    if let Err(err) = res {
        println!("{err:?}");
    }

    Ok(())
}

fn run_app<B: Backend>(
    terminal: &mut Terminal<B>,
    mut app: App,
    tick_rate: Duration,
) -> io::Result<()> {
    let mut last_tick = Instant::now();
    loop {
        terminal.draw(|f| ui(f, &mut app))?;

        let timeout = tick_rate
            .checked_sub(last_tick.elapsed())
            .unwrap_or_else(|| Duration::from_secs(0));

        // Read keyboard events
        if event::poll(timeout)? {
            if let Event::Key(key) = event::read()? {
                match app.mode {
                    // When the app is in drawing mode
                    Mode::Drawing => match key.code {
                        // Quit
                        KeyCode::Char('q') => {
                            app.mode = Mode::Quit;
                        }

                        // Save
                        KeyCode::Char('w') => {
                            // save(app.pixels.clone())?;
                            app.mode = Mode::Save;
                        }

                        // Movement [hjkl or ←↓↑→]
                        KeyCode::Char('k') | KeyCode::Up => {
                            if app.y < Y_SIZE {
                                app.y += 1.0;
                            }
                        }
                        KeyCode::Char('j') | KeyCode::Down => {
                            if app.y > 1.0 {
                                app.y -= 1.0;
                            }
                        }
                        KeyCode::Char('l') | KeyCode::Right => {
                            if app.x < X_SIZE {
                                app.x += 1.0;
                            }
                        }
                        KeyCode::Char('h') | KeyCode::Left => {
                            if app.x > 1.0 {
                                app.x -= 1.0;
                            }
                        }

                        // Draw, erase and color grab
                        KeyCode::Char(' ') => {
                            app.pixels.insert((app.x as i64, app.y as i64), app.color);
                        }
                        KeyCode::Backspace => {
                            app.pixels.insert((app.x as i64, app.y as i64), (0, 0, 0));
                        }
                        KeyCode::Char('i') => {
                            app.color = *app.pixels.get(&(app.x as i64, app.y as i64)).unwrap_or(&(00, 00, 00));
                        }

                        // Change to color select mode
                        KeyCode::Char('b') => {
                            app.mode = Mode::ColorChange;
                        }
                        _ => {}
                    },

                    // When the app is in color select mode
                    Mode::ColorChange => match key.code {
                        // If enter is pressed and the message is 6 characters long, parse it from
                        // a hex color value to an rbg value and set the current brush color value
                        KeyCode::Enter => {
                            let color: &str = app.color_input.get(..).unwrap_or("000000");
                            if color.len() == 6 {
                                let r = <[u8; 1]>::from_hex(color.get(0..2).unwrap_or("00"))
                                    .map_or(0, |v| v[0]);
                                let g = <[u8; 1]>::from_hex(color.get(2..4).unwrap_or("00"))
                                    .map_or(0, |v| v[0]);
                                let b = <[u8; 1]>::from_hex(color.get(4..6).unwrap_or("00"))
                                    .map_or(0, |v| v[0]);
                                app.color = (r, g, b);
                            }
                        }

                        // If a valid (0..F, the hex numbers) character is input, add it to the
                        // color input string
                        KeyCode::Char(c) => {
                            if app.color_input.len() < 6 {
                                match c {
                                    '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | '0'
                                    | 'a' | 'A' | 'b' | 'B' | 'c' | 'C' | 'd' | 'D' | 'e' | 'E'
                                    | 'f' | 'g' => app.color_input.push(c),
                                    _ => {}
                                }
                            }
                        }

                        // Regular backspace vehavior
                        KeyCode::Backspace => {
                            app.color_input.pop();
                        }

                        // Change to drawing mode when Esc is pressed
                        KeyCode::Esc => {
                            app.mode = Mode::Drawing;
                        }
                        _ => {}
                    },

                    // When the user is trying to save
                    Mode::Save => match key.code {
                        // If any character key is pressed, add it to the filename
                        KeyCode::Char(c) => app.filename.push(c),

                        // Regular backspace behaviour
                        KeyCode::Backspace => {
                            app.filename.pop();
                        }

                        // Cancel
                        KeyCode::Esc => {
                            app.mode = Mode::Drawing;
                        }

                        // If enter is pressed and the user has typed a filename, save the drawing
                        KeyCode::Enter => {
                            if !app.filename.is_empty() {
                                app.filename.push_str(".rs");
                                save(app.pixels.clone(), &app.filename).unwrap();
                                app.filename.clear();
                                app.mode = Mode::Drawing;
                            }
                        }
                        _ => {}
                    },

                    // When the user wants to quit, only quit if 'y' is pressed
                    Mode::Quit => match key.code {
                        KeyCode::Char('y') => return Ok(()),
                        _ => app.mode = Mode::Drawing,
                    },
                }
            }
        }

        if last_tick.elapsed() >= tick_rate {
            last_tick = Instant::now();
        }
    }
}

fn ui<B: Backend>(f: &mut Frame<B>, app: &mut App) {
    // UI distribution
    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints(
            [
                Constraint::Percentage(90),
                Constraint::Percentage(75),
                Constraint::Percentage(25),
            ]
            .as_ref(),
        )
        .split(f.size());

    // User drawing canvas
    let canvas_container = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([Constraint::Ratio(15, 9), Constraint::Ratio(15, 9)].as_ref())
        .split(chunks[0]);
    let canvas = Canvas::default()
        .block(Block::default().borders(Borders::ALL))
        .paint(|ctx| {
            // User drawing. Draws every dot on the canvas individually
            for pixel in &app.pixels {
                let x = pixel.0 .0 as f64;
                let y = pixel.0 .1 as f64;
                let (r, g, b) = *pixel.1;

                ctx.print(
                    x,
                    y,
                    Span::styled("●", Style::default().fg(Color::Rgb(r, g, b))),
                );
            }

            // Cursor
            ctx.draw(&Rectangle {
                x: app.x - 0.25,
                y: app.y - 0.25,
                width: 0.5,
                height: 0.5,
                color: Color::White,
            });
        })
        .x_bounds([0.0, 16.0])
        .y_bounds([0.0, 10.0]);
    f.render_widget(canvas, canvas_container[0]);

    // Footer
    let footer = Canvas::default()
        .block(Block::default().borders(Borders::ALL))
        .paint(|ctx| {
            let (r, g, b) = app.color;
            ctx.print(20.0, 5.0, "[Space] Draw");
            ctx.print(35.0, 5.0, "[Backspace] Erase");
            ctx.print(50.0, 5.0, "[i] Grab color");
            ctx.print(65.0, 5.0, "[b] Select color");
            ctx.print(80.0, 5.0, "[w] Write to file");
            ctx.print(95.0, 5.0, "[q] Quit");
            ctx.print(
                10.0,
                05.0,
                Span::styled("      ", Style::default().bg(Color::Rgb(r, g, b))),
            );
        })
        .x_bounds([0.0, 100.0])
        .y_bounds([0.0, 10.0]);
    f.render_widget(footer, chunks[1]);

    // Footer title
    let (msg, style) = match app.mode {
        Mode::Drawing => (
            vec![Span::raw("Drawing mode")],
            Style::default().add_modifier(Modifier::RAPID_BLINK),
        ),
        Mode::ColorChange => (vec![Span::raw("Color select mode")], Style::default()),
        Mode::Save => (vec![Span::raw("Saving...")], Style::default()),
        Mode::Quit => (vec![Span::raw("Quit?")], Style::default()),
    };
    let mut text = Text::from(Spans::from(msg));
    text.patch_style(style);
    let help_message = Paragraph::new(text);
    f.render_widget(help_message, chunks[1]);

    // 6 character hex color input field
    let input = Paragraph::new(app.color_input.as_ref())
        .style(match app.mode {
            Mode::ColorChange => Style::default().fg(Color::Cyan),
            _ => Style::default(),
        })
        .block(Block::default().borders(Borders::ALL));
    f.render_widget(
        input,
        tui::layout::Rect {
            x: chunks[1].x + 1,
            y: chunks[1].y + 1,
            width: 8,
            height: 3,
        },
    );
    

    let area = centered_rect(60, 10, f.size());
    let block = Block::default().borders(Borders::ALL);

    match app.mode {
        Mode::Drawing => {} // Don't do anything special on drawing mode

        // Move cursor according to text on color change mode
        Mode::ColorChange => {
            f.set_cursor(
                chunks[1].x
                    + if app.color_input.width() < 6 {
                        app.color_input.width() as u16 + 2
                    } else {
                        app.color_input.width() as u16 + 1
                    },
                chunks[1].y + 2,
            );
        }
        
        // On save mode, create a popup asking the user for a filename
        Mode::Save => {
            let info = Canvas::default()
                .block(Block::default().borders(Borders::ALL))
                .paint(|ctx| {
                    ctx.print(00.0, 00.0, "[Enter] Write to file");
                    ctx.print(30.0, 00.0, "[Esc] Cancel");
                    ctx.print(0.0, 10.0, "File name: ");
                    ctx.print(10.0, 10.0, app.filename.clone());
                })
                .x_bounds([0.0, 100.0])
                .y_bounds([0.0, 10.0]);
            
            block.clone().title("Saving...");
            f.set_cursor(area.x + app.filename.width() as u16 + 12, area.y + 1);

            f.render_widget(Clear, area); // this clears out the background
            f.render_widget(info, area);
            f.render_widget(block, area);
        }

        // On quit mode, make the user confirm if they want to quit
        Mode::Quit => {
            let info = Canvas::default()
                .block(Block::default().borders(Borders::ALL))
                .paint(|ctx| {
                    ctx.print(00.0, 10.0, "All unsaved changes will be lost. Are you sure you want to quit?");
                    ctx.print(00.0, 00.0, "[y] Yes");
                    ctx.print(50.0, 00.0, "[n] No");
                })
                .x_bounds([0.0, 100.0])
                .y_bounds([0.0, 10.0]);
            
            block.clone().title("Quit?");
            f.render_widget(Clear, area); // this clears out the background
            f.render_widget(info, area);
            f.render_widget(block, area);
        }
    }
}

// Save the drawing hashmap to a file as an array.
fn save(drawing: HashMap<(i64, i64), (u8, u8, u8)>, filename: &str) -> Result<(), std::io::Error> {
    // Start of the file. Formats it as a valid rust array.
    let mut file_contents: String = format!(
        "let image: [(usize, usize, u8, u8, u8); {}] = [",
        X_SIZE * Y_SIZE
    );

    // Pushes each pixel in the hashmap to the array in the appropriate format
    for (k, v) in drawing {
        let (x, y, r, g, b) = (k.0 as usize - 1, k.1 as usize - 1, v.0, v.1, v.2);
        let pixel = format!("({x}, {y}, {r}, {g}, {b}), ");
        file_contents.push_str(&pixel);
    }
    file_contents.push_str("];");       // File ending

    write(filename, file_contents)?;    // Will create a new file or replace an existing one
    Ok(())
}

// Create a popup rectangle
fn centered_rect(percent_x: u16, percent_y: u16, r: Rect) -> Rect {
    let popup_layout = Layout::default()
        .direction(Direction::Vertical)
        .constraints(
            [
                Constraint::Percentage((100 - percent_y) / 2),
                Constraint::Percentage(percent_y),
                Constraint::Percentage((100 - percent_y) / 2),
            ]
            .as_ref(),
        )
        .split(r);

    Layout::default()
        .direction(Direction::Horizontal)
        .constraints(
            [
                Constraint::Percentage((100 - percent_x) / 2),
                Constraint::Percentage(percent_x),
                Constraint::Percentage((100 - percent_x) / 2),
            ]
            .as_ref(),
        )
        .split(popup_layout[1])[1]
}
