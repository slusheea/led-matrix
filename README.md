# LED Matrix display

![creature](images/creature.gif)

## TUI Draw
With TUI Draw, you can draw anything for the matrix display

![gitlab_drawing](images/gitlab_drawing.png)

Move around with the arrow keys or vim keys, draw with the space bar and erase with backspace.
### Pick your colors
![color_picker](images/color_input.png)

Choose what color you want to draw with. Type it in in hex format or paste it from anywhere.
### Save it for later
![save_file](images/save_file.png)

Save your drawing as a valid rust array to use it with the led matrix

## Firmware
The firmware allows you to display any image you've previously drawn with `tui_draw`.
The included example of the firmware consists on a little "creature" that makes different faces depending on what it senses.

The firmware is configured to use the SPI pins 6, 7 and 8 on the Pico and have the LED strip wired with the shortest path wire.

![wiring](images/wiring.png)
